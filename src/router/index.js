import Vue from 'vue'
import Router from 'vue-router'
import loginPage from '@/pages/login/_LoginPage'
import signupPage from '@/pages/login/_SignupPage'
import homePage from '@/pages/home/_HomePage'
import jobpostPage from '@/pages/jobpost/_JobpostPage'
import joblistPage from '@/pages/joblist/_JoblistPage'

import BookStore from '../components/BookStore';
import MakeBook from '../components/MakeBook';
import Step1 from '../components/Step1';
import Step2 from '../components/Step2';
import Step3 from '../components/Step3';
import Step4 from '../components/Step4';
import Step5 from '../components/Step5';
import Step6 from '../components/Step6';
import About from '../components/About';
import BoardList from '../components/BoardList';
import BoardForm from '../components/BoardForm';

Vue.use(Router)

export default new Router({
    mode: 'history',
    routes: [
        { path: '/', redirect: '/home' },
        { path: '/home', name: 'home', component: homePage },
        { path: '/make', name: 'make', component: MakeBook },
        { path: '/step1', name: 'step1', component: Step1 },
        { path: '/step2', name: 'step2', component: Step2 },
        { path: '/step3', name: 'step3', component: Step3 },
        { path: '/step4', name: 'step4', component: Step4 },
        { path: '/step5', name: 'step5', component: Step5 },
        { path: '/step6', name: 'step5', component: Step6 },
        { path: '/about', name: 'about', component: About },
        { path: '/store', name: 'store', component: BookStore },
        {
            path: '/boards',
            name: 'boards',
            component: BoardList,
            children: [
                { path: 'add', name: 'addboard', component: BoardForm },
                { path: 'update/:id', name: 'updateboard', component: BoardForm, props: true },
            ]
        },
        {
            path: '/login',
            name: 'login',
            component: loginPage
        },
        {
            path: '/signup',
            name: 'signup',
            component: signupPage
        },
        {
            path: '/job-post',
            name: 'jobpost',
            component: jobpostPage
        },
        {
            path: '/job-list',
            name: 'joblist',
            component: joblistPage
        }
    ]
})