import boardAPI from '../api/BoardsAPI';
import Constant from '../constant';
import CONF from '../Config';

export default {
    [Constant.FETCH_BOARDS]: (store, payload) => {
        var pageno;
        if (typeof payload === "undefined" || typeof payload.pageno === "undefined")
            pageno = 0;
        else
            pageno = payload.pageno - 1;

        var pagesize = store.state.boardlist.pagesize;

        boardAPI.fetchBoards(pageno, pagesize)
            .then((response) => {
                store.commit(Constant.FETCH_BOARDS, {
                    pageno: response.data.number,
                    pagesize: CONF.PAGESIZE,
                    totalcount: response.data.totalElements,
                    boards: response.data.content
                });
            })
    },
    [Constant.ADD_BOARD]: (store) => {
        boardAPI.addBoard(store.state.board)
            .then((response) => {
                if (response.data.status == "success") {
                    store.dispatch(Constant.CANCEL_FORM);
                    store.dispatch(Constant.FETCH_BOARDS, { pageno: 1 });
                } else {
                    console.log("게시물 추가 실패 : " + response.data);
                }
            })
    },
    [Constant.UPDATE_BOARD]: (store) => {
        var currentPageNo = store.state.boardlist.pageno;
        boardAPI.updateBoard(store.state.board)
            .then((response) => {
                store.dispatch(Constant.FETCH_BOARDS, { pageno: currentPageNo });
            })
    },
    [Constant.DELETE_BOARD]: (store, payload) => {
        var currentPageNo = store.state.boardlist.pageno;
        boardAPI.deleteBoard(payload.id)
            .then((response) => {
                store.dispatch(Constant.FETCH_BOARDS, { pageno: currentPageNo });
            })
    },
    [Constant.FETCH_BOARD_ONE]: (store, payload) => {
        boardAPI.fetchBoardOne(payload.id)
            .then((response) => {
                store.commit(Constant.FETCH_BOARD_ONE, { board: response.data });
            })
    },
    [Constant.INITIALIZE_BOARD_ONE]: (store) => {
        store.commit(Constant.INITIALIZE_BOARD_ONE);
    }
}