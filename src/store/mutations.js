import Constant from '../constant';

export default {
    [Constant.FETCH_BOARDS]: (state, payload) => {

        state.boardlist.pageno = payload.pageno - 1;
        state.boardlist.pagesize = payload.pagesize;
        state.boardlist.totalcount = payload.totalcount;
        state.boardlist.boards = payload.boards;
    },
    [Constant.FETCH_BOARD_ONE]: (state, payload) => {
        state.board = payload.board;
    },
    [Constant.INITIALIZE_BOARD_ONE]: (state) => {
        state.board = { id: '', title: '', name: '', date: '', document: '' };
    }
}