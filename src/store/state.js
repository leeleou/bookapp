import Constant from '../constant';
import CONF from '../Config.js';

export default {
    board: { id: 0, title: '', name: '', date: '', document: '' },
    boardlist: {
        pageno: 1,
        pagesize: CONF.PAGESIZE,
        totalcount: 0,
        boards: []
    }
}