import CONF from '../Config.js';
import axios from 'axios';

export default {
    fetchBoards: function(pageno, pagesize) {
        return axios.get(CONF.FETCH, {
            params: {
                page: pageno,
                size: pagesize,
                sort: "Id,desc"
            }
        })
    },
    fetchBoardOne: function(no) {
        return axios.get(CONF.FETCH_ONE + no)
    },
    addBoard: function(board) {
        return axios.post(CONF.ADD, board);
    },
    updateBoard: function(board) {
        return axios.post(CONF.UPDATE, board);
    },
    deleteBoard: function(no) {
        return axios.delete(CONF.DELETE + no);
    }
}