var BASE_URL = "/api";

export default {
    PAGESIZE: 5, //한 페이지에 보여줄 페이지 사이즈
    //전체 게시판 데이터 요청(페이징 포함)
    FETCH: BASE_URL + "/boards",
    //게시 데이터 추가
    ADD: BASE_URL + "/boards",
    //게시물 업데이트
    UPDATE: BASE_URL + "/boards",
    //게시물 한건 조회
    FETCH_ONE: BASE_URL + "/board?id=",
    //게시물 삭제
    DELETE: BASE_URL + "/boards?id=",
    //게시물 업데이트
    UPDATE_DOC: BASE_URL + "/boards/${id}/document"
}