/*eslint-disable */
export const data = {
    hotJobs: [
        { title: '간호인력1', desc: '거동불편 노인 간호', location: '서울 서초구 방배동', time: 'Full Time', minPay: '25000', maxPay: '35000', skill: '요양보호사' },
        { title: '간호인력2', desc: '거동불편 노인 간호', location: '서울 서초구 방배동', time: 'Full Time', minPay: '25000', maxPay: '35000', skill: '요양보호사' },
        { title: '간호인력3', desc: '거동불편 노인 간호', location: '서울 서초구 방배동', time: 'Full Time', minPay: '25000', maxPay: '35000', skill: '요양보호사' },
        { title: '간호인력4', desc: '거동불편 노인 간호', location: '서울 서초구 방배동', time: 'Full Time', minPay: '25000', maxPay: '35000', skill: '요양보호사' }
    ],
    recentJobs: [
        { title: '간호인력11', desc: '거동불편 노인 간호', location: '서울 서초구 방배동', time: 'Full Time', minPay: '25000', maxPay: '35000', skill: '요양보호사' },
        { title: '간호인력22', desc: '거동불편 노인 간호', location: '서울 서초구 방배동', time: 'Full Time', minPay: '25000', maxPay: '35000', skill: '요양보호사' },
        { title: '간호인력33', desc: '거동불편 노인 간호', location: '서울 서초구 방배동', time: 'Full Time', minPay: '25000', maxPay: '35000', skill: '요양보호사' },
        { title: '간호인력44', desc: '거동불편 노인 간호', location: '서울 서초구 방배동', time: 'Full Time', minPay: '25000', maxPay: '35000', skill: '요양보호사' }
    ],
    popularJobs: [
        { title: '간호인력111', desc: '거동불편 노인 간호', location: '서울 서초구 방배동', time: 'Full Time', minPay: '25000', maxPay: '35000', skill: '요양보호사' },
        { title: '간호인력222', desc: '거동불편 노인 간호', location: '서울 서초구 방배동', time: 'Full Time', minPay: '25000', maxPay: '35000', skill: '요양보호사' },
        { title: '간호인력333', desc: '거동불편 노인 간호', location: '서울 서초구 방배동', time: 'Full Time', minPay: '25000', maxPay: '35000', skill: '요양보호사' },
        { title: '간호인력444', desc: '거동불편 노인 간호', location: '서울 서초구 방배동', time: 'Full Time', minPay: '25000', maxPay: '35000', skill: '요양보호사' }
    ]
}

export const statistics = {
    stats: [
        { info: "승인된 도서", count: "5,197", desc: "BOOK에 승인된 도서의 개수입니다." },
        { info: "제작중인 도서", count: "6,305", desc: "BOOK에서 지금 만들어지고 있는 도서의 개수입니다." },
        { info: "활동중인 작가", count: "5,050", desc: "BOOK에서 많은 작가분들이 활동하고 있습니다." },
        { info: "등록된 전자책", count: "1,128", desc: "BOOK의 다양한 전자챍을 무료로 체험할 수 있습니다." }
    ]
}