export default {
    FETCH_BOARDS: "fetchBoards",
    ADD_BOARD_FORM: "addBoardForm",
    ADD_BOARD: "addBoard",
    EDIT_BOARD_FORM: "editBoardForm",
    UPDATE_BOARD: "updateBoard",
    CANCEL_FORM: "cancelForm",
    DELETE_BOARD: "deleteBoard",
    FETCH_BOARD_ONE: "fetchBoardOne",
    INITIALIZE_BOARD_ONE: "initializeBoardOne"
}